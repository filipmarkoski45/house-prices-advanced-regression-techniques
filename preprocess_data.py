import os

import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import RobustScaler

from data_cleaning import clean_data
from definitions import serialized_feather, serialized_data_sets, timer
from definitions import serialized_train_feather, serialized_valid_feather, serialized_X_test_feather
from definitions import training_set_file_path, testing_set_file_path
from pd_utils import downcast_dtypes, create_namedtuple
# Local Imports
from pd_utils import split_class, split_data_frame, df_info


def load_data_as_numpy(train=None, test=None, valid=None, have_valid=False,
                       scaler=RobustScaler(), scale=False,
                       display_shapes=False, pca_analyse=False):
    # Load the data sets as pd.DataFrame's
    if have_valid:
        train, valid, test = load_data(have_valid=have_valid)

        train.X.drop(columns=['Id'], axis=1, inplace=True)
        valid.X.drop(columns=['Id'], axis=1, inplace=True)

        valid.X = valid.X.to_numpy()
        valid.y = valid.y.to_numpy()
    elif not have_valid and (train is None or test is None):
        train, test = load_data(have_valid=have_valid)
        train.X.drop(columns=['Id'], axis=1, inplace=True)

    test.X.drop(columns=['Id'], axis=1, inplace=True)

    # test.y is not important because it was never provided

    # Transform the dataframes to numpy arrays
    train.X = train.X.to_numpy()
    train.y = train.y.to_numpy()
    test.X = test.X.to_numpy()

    if display_shapes:
        print(train.X.shape)
        print(train.y.shape)
        if have_valid:
            print(valid.X.shape)
            print(valid.y.shape)
        print(test.X.shape)

    if scale:
        scaler.fit(train.X)
        train.X = scaler.transform(train.X)
        if have_valid:
            valid.X = scaler.transform(valid.X)
        test.X = scaler.transform(test.X)

    if pca_analyse:
        print('With PCA')
        pca = PCA(n_components=410)
        train.X = pca.fit_transform(train.X)
        if have_valid:
            valid.X = pca.transform(valid.X)
        test.X = pca.transform(test.X)

    if have_valid:
        return train, valid, test
    return train, test


def adjust_for_inflation(train_data):
    inflation = pd.DataFrame(dict(value_by_1860_usd=[24.29, 24.98, 25.94, 25.85, 26.27],
                                  inflation_percent=[3.23, 2.85, 3.84, -0.36, 1.64]),
                             index=['2006', '2007', '2008', '2009', '2010'])

    # Let's start by creating a new data set for each year:
    infl_df_2006 = train_data.loc[train_data['YrSold'] == 2006]
    infl_df_2007 = train_data.loc[train_data['YrSold'] == 2007]
    infl_df_2008 = train_data.loc[train_data['YrSold'] == 2008]
    infl_df_2009 = train_data.loc[train_data['YrSold'] == 2009]
    infl_df_2010 = train_data.loc[
        train_data['YrSold'] == 2010]  # this one will not be changed, just used for final concat

    # Since we tried adjusting prices for inflation, and the MAE turned out ot be worse,
    # we are going to try at different % of inflation correction: 25%, 50%, 75% and 100%

    # 25%
    infl_df_2006_25 = infl_df_2006
    infl_df_2007_25 = infl_df_2007
    infl_df_2008_25 = infl_df_2008
    infl_df_2009_25 = infl_df_2009
    infl_df_2010_25 = infl_df_2010

    # We get the value of the USD at a given year
    usd_2006 = inflation.loc['2006', 'value_by_1860_usd']
    usd_2007 = inflation.loc['2007', 'value_by_1860_usd']
    usd_2008 = inflation.loc['2008', 'value_by_1860_usd']
    usd_2009 = inflation.loc['2009', 'value_by_1860_usd']
    usd_2010 = inflation.loc['2010', 'value_by_1860_usd']

    # We then get a factor of the value of the USD in a year compared to the 2010 USD:
    divid_factor_06 = usd_2010 / usd_2006
    divid_factor_07 = usd_2010 / usd_2007
    divid_factor_08 = usd_2010 / usd_2008
    divid_factor_09 = usd_2010 / usd_2009

    print('divide factor for 2006 is {} '.format(divid_factor_06))
    print('divide factor for 2007 is {} '.format(divid_factor_07))
    print('divide factor for 2008 is {} '.format(divid_factor_08))
    print('divide factor for 2009 is {} '.format(divid_factor_09))

    # 25%:
    divid_factor_06_25 = divid_factor_06 / 4
    divid_factor_07_25 = divid_factor_07 / 4
    divid_factor_08_25 = divid_factor_08 / 4
    divid_factor_09_25 = divid_factor_09 / 4

    # We need to multiply the values of SalePrice by divid_factor

    # 25%:
    infl_df_2006_25['SalePrice'] = infl_df_2006_25['SalePrice'].multiply(divid_factor_06_25)
    infl_df_2007_25['SalePrice'] = infl_df_2007_25['SalePrice'].multiply(divid_factor_07_25)
    infl_df_2008_25['SalePrice'] = infl_df_2008_25['SalePrice'].multiply(divid_factor_08_25)
    infl_df_2009_25['SalePrice'] = infl_df_2009_25['SalePrice'].multiply(divid_factor_09_25)

    # Now we need to concat all the sub DataFrames in one bigger one
    # 25%:
    frames_25 = [infl_df_2006_25, infl_df_2007_25, infl_df_2008_25, infl_df_2009_25, infl_df_2010_25]
    infl_train_data_25 = pd.concat(frames_25)

    print('size of train data : ', train_data.size, train_data.shape)
    print('size of train data adjusted for inflation : ', infl_train_data_25.size, infl_train_data_25.shape)

    features_to_sort_by = ['Id']
    infl_train_data_25.sort_values(features_to_sort_by, ascending=[True], inplace=True)

    print(train_data.head())
    print(infl_train_data_25.head())
    return infl_train_data_25


def basic_train_preprocessing(df: pd.DataFrame, class_feature):
    # removing outliers according to the author's suggestion
    # outliers are the rows with Id = [1299, 524]
    selection = (df['GrLivArea'] > 4000) & (df['SalePrice'] < 300000)
    df.drop(df[selection].index, inplace=True)

    # Resetting the index, VERY important
    df.reset_index(drop=True, inplace=True)

    # Adjusting for inflation (DOES NOT WORK)
    # df = adjust_for_inflation(df)

    # https://stats.stackexchange.com/a/40918/254676
    df[class_feature] = np.log(df[class_feature])

    return df


def feature_preprocessing(X_train: pd.DataFrame, X_test: pd.DataFrame, path_to_feather: str,
                          serialize: bool = True, get_raw_data: bool = False):
    train_ids = X_train['Id'].copy()
    test_ids = X_test['Id'].copy()

    data = pd.concat((X_train, X_test), ignore_index=True, sort=False)

    # data_cleaning.py
    data = clean_data(data)

    # Storing the data to a feather format
    if serialize:
        data = downcast_dtypes(data)
        data.to_feather(path_to_feather)

    if get_raw_data:
        return data

    # Splitting the data back into the training set and the test set
    X_train = data.loc[data['Id'].isin(train_ids)].copy()
    X_test = data.loc[data['Id'].isin(test_ids)].copy()

    return X_train, X_test


def load_data(shuffle=True, serialize=False, get_raw_data=False, have_valid=False, latest_data_as_valid=False):
    class_feature = 'SalePrice'

    if get_raw_data == False and serialize == False and all([os.path.isfile(file) for file in serialized_data_sets]):
        print("Reading data sets from feather format...")

        train = pd.read_feather(serialized_train_feather)
        X_test = pd.read_feather(serialized_X_test_feather)

        X_train, y_train = split_class(train, class_feature)

        if have_valid:
            X_valid = pd.read_feather(serialized_valid_feather)
            X_valid, y_valid = split_class(X_valid, class_feature)

    elif get_raw_data == True and serialize == False and os.path.isfile(serialized_feather):
        print("Reading data from feather format...")
        data = pd.read_feather(serialized_feather)
        return data
    else:
        print("No feather format found. Generating pd.DataFrame...")

        train = pd.read_csv(training_set_file_path, low_memory=False)
        X_test = pd.read_csv(testing_set_file_path, low_memory=False)

        train = basic_train_preprocessing(train, class_feature)

        X_train, y_train = split_class(train, class_feature)
        X_train, X_test = feature_preprocessing(X_train, X_test, serialized_feather)

        # print(f'X_train.shape={X_train.shape}')
        # print(f'X_test.shape={X_test.shape}')

        if serialize:
            df_info(X_train)
            df_info(X_test)

        # We have to append the class feature to other training features
        # In order to serialize them as a whole
        # and maybe generate a validation set
        train = X_train  # .copy()
        train[class_feature] = y_train

        if have_valid:
            # Getting the latest training data to serve as a validation set
            if latest_data_as_valid:
                features_to_sort_by = ['YearBuilt']
                train.sort_values(features_to_sort_by, ascending=[True], inplace=True)
            train, valid = split_data_frame(train, percent=0.90, shuffle=shuffle)

            valid.to_feather(serialized_valid_feather)
            X_valid, y_valid = split_class(valid, class_feature)

        # print(f'train.shape={train.shape}')

        # print(f'Dataframe before downcasting dtypes:')
        # print(X_test.info())

        # Reducing the overall memory by optimizing the data types
        train = downcast_dtypes(train)
        X_test = downcast_dtypes(X_test)

        # print(f'Dataframe after downcasting dtypes:')
        # print(X_test.info())

        # Resetting the dataframe index in order to comply with feather
        train.reset_index(drop=True, inplace=True)
        X_test.reset_index(drop=True, inplace=True)

        # Serializing and storing each data set
        train.to_feather(serialized_train_feather)
        X_test.to_feather(serialized_X_test_feather)

        X_train, y_train = split_class(train, class_feature)

    # Packaging the Data
    train = create_namedtuple(X_train, y_train)
    test = create_namedtuple(X_test, None)

    if have_valid:
        valid = create_namedtuple(X_valid, y_valid)
        return train, valid, test

    return train, test


@timer
def main():
    serialize = True
    have_valid = False

    if have_valid:
        train, valid, test = load_data(serialize=serialize, have_valid=have_valid)

        print(f'his X is =(2917, {426 + 1 })')

        print(f'train.X.shape={train.X.shape}')
        print(f'train.y.shape={train.y.shape}')
        print(f'valid.X.shape={valid.X.shape}')
        print(f'valid.y.shape={valid.y.shape}')
        print(f'test.X.shape={test.X.shape}')

        difference = set(train.X.columns).symmetric_difference(set(test.X.columns))
        print(difference)

        print(valid.X[['Id', 'YearBuilt']].head(5))
        print(valid.X['YearBuilt'].describe())
        print(valid.X.head(5))
    else:
        train, test = load_data(serialize=serialize)
        print(f'train.X.shape={train.X.shape}')
        print(f'train.y.shape={train.y.shape}')

        print(f'test.X.shape={test.X.shape}')

        print(train.X.info())
        print(test.X.info())


if __name__ == '__main__':
    main()
