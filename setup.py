# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

# with open('LICENSE') as f:
#     license = f.read()

setup(
    name='house-prices-advanced-regression-techniques',
    version='0.1.0',
    description='Sample package for Python-Guide.org',
    long_description=readme,
    author='Filip Markoski',
    author_email='filip.markoski45@gmail.com',
    url='https://gitlab.com/filipmarkoski45/house-prices-advanced-regression-techniques',
    # license=license,
    packages=find_packages(exclude=('tests', 'docs', 'data'))
)
