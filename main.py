from operator import itemgetter

import lightgbm as lgb
import numpy as np
import pandas as pd
# Regression Models
import xgboost as xgb
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.linear_model import ElasticNetCV, ElasticNet
from sklearn.linear_model import HuberRegressor, PassiveAggressiveRegressor, SGDRegressor
from sklearn.linear_model import Lasso, LassoCV
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import RANSACRegressor
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.svm import SVR

from definitions import RNG_SEED, SERIALIZED_DIR, timer
from ensemble_models import AveragingModels, StackingModels, BestModels
from metrics import feature_importances, rmsle_cv, print_score, make_grid
# from neural_network_models import nn_start_small_approach
from preprocess_data import load_data, load_data_as_numpy


# Local imports

def multi_approach(train):
    alphas = [0.0001, 0.0004, 0.0008, 0.001, 0.004, 0.008, 0.01, 0.04, 0.08, 0.1, 0.4, 0.8, 1]
    n_estimators = 300

    model_names = ['LinearRegression',
                   'HuberRegressor',
                   'PassiveAggressiveRegressor',
                   'SGDRegressor',
                   # 'TheilSenRegressor',
                   'RANSACRegressor',
                   'LassoCV',
                   'ElasticNetCV',
                   'RidgeCV',
                   'KernelRidge',
                   'GradientBoostingRegressor',
                   'RandomForestRegressor',
                   'XGBRegressor',
                   'LGBMRegressor'
                   ]
    models = [LinearRegression(normalize=True),
              HuberRegressor(),
              PassiveAggressiveRegressor(),
              SGDRegressor(),
              # TheilSenRegressor(),
              RANSACRegressor(),
              LassoCV(
                  alphas=alphas,
                  max_iter=70000,
                  cv=10,
                  normalize=True
              ),
              ElasticNetCV(
                  alphas=alphas,
                  l1_ratio=.9,
                  random_state=RNG_SEED,
                  cv=10,
                  normalize=True
              ),
              RidgeCV(
                  alphas=alphas,
                  normalize=True
              ),
              KernelRidge(
                  alpha=0.6, kernel='polynomial', degree=2, coef0=2.5
              ),
              GradientBoostingRegressor(
                  n_estimators=n_estimators, learning_rate=0.05,
                  max_depth=4, max_features='sqrt',
                  min_samples_leaf=15, min_samples_split=10,
                  loss='huber', random_state=RNG_SEED
              ),
              RandomForestRegressor(
                  n_estimators=n_estimators,
                  min_samples_leaf=3,
                  max_features=0.5,
                  # n_jobs=-1,  # use all the cores you can
                  oob_score=True),
              xgb.XGBRegressor(objective='reg:squarederror',
                               n_estimators=n_estimators,
                               learning_rate=0.05,
                               random_state=RNG_SEED,
                               n_jobs=-1,
                               silent=True),
              lgb.LGBMRegressor(objective='regression', num_leaves=5,
                                learning_rate=0.05, n_estimators=n_estimators,
                                max_bin=55, bagging_fraction=0.8,
                                bagging_freq=5, feature_fraction=0.2319,
                                feature_fraction_seed=9, bagging_seed=9,
                                min_data_in_leaf=6, min_sum_hessian_in_leaf=11,
                                silent=True)
              ]

    score_board = dict()
    for model_name, model in zip(model_names, models):
        print(f'{model_name} Approach')
        score_board[model_name] = rmsle_cv(model, train)
        # model.fit(train.X, train.y)
        # print_score(model, train, valid)

    print('Score Board:')
    score_board = sorted(score_board.items(), key=itemgetter(1))
    for k, v in score_board:
        print(f'\t{k}={v}')


def lasso_cv_approach(train, valid, test):
    print('Lasso Cross-Validation Approach')
    model = LassoCV(
        alphas=[0.0001, 0.0004, 0.0008, 0.001, 0.004, 0.008, 0.01, 0.04, 0.08, 0.1, 0.4, 0.8, 1],
        max_iter=70000,
        cv=10,
        normalize=True)
    model.fit(train.X, train.y)
    print_score(model, train, valid)
    return model


def random_forrest_approach(train, valid, test, ispd=False):
    print('RandomForestRegressor Approach')
    model = RandomForestRegressor(
        n_estimators=40,
        min_samples_leaf=3,
        max_features=0.5,
        n_jobs=-1,  # use all the cores you can
        oob_score=True)

    model.fit(train.X, train.y)
    print_score(model, train, valid)

    if ispd:
        targets = valid.y.to_numpy()
        predictions = model.predict(valid.X)

        feature_list = valid.X.columns.to_list()

        print('Feature importance')
        features_to_keep = feature_importances(model, feature_list)

        return features_to_keep
    return model


def kernel_ridge_approach(train, valid=None):
    print('KernelRidge Approach')

    # alpha=0.33, kernel='polynomial', degree=3, coef0=1
    model = KernelRidge()
    param_grid = {
        'alpha': np.around(np.linspace(0.35, 0.5, 15), decimals=3),
        'kernel': ['polynomial'],
        'degree': [3],
        'coef0': [1]
    }

    return make_grid(model, train, param_grid)


def lasso_approach(train, valid=None):
    print('Lasso Approach')

    model = Lasso()
    alphas = [0.01, 0.04, 0.08, 0.1, 0.4, 0.8, 1]
    alphas.extend(list(np.around(np.linspace(0.0001, 0.001, 10), decimals=4)))
    param_grid = {
        'alpha': alphas,
        'max_iter': [10000, 100000]
    }

    return make_grid(model, train, param_grid)


def ridge_approach(train):
    print('Ridge Approach')

    model = Ridge()
    # alphas = np.around(np.arange(0, 100, 5), decimals=4)
    alphas = np.around(np.linspace(50, 60, 20), decimals=2)
    param_grid = {
        'alpha': alphas,
    }

    return make_grid(model, train, param_grid)


def svr_approach(train):
    print('SVR Approach')

    # SVR(C=11, epsilon=0.01, gamma=0.0004, kernel='rbf')
    model = SVR()
    param_grid = {
        'C': [8, 9, 10, 11],
        'kernel': ["rbf"],
        "gamma": [0.0003, 0.0004, 0.0005, 0.0006],
        "epsilon": [0.008, 0.009, 0.01, 0.02, 0.03]
    }

    return make_grid(model, train, param_grid)


def elastic_net_approach(train):
    print('ElasticNet Approach')

    model = ElasticNet()
    alphas = [0.01, 0.04, 0.08, 0.1, 0.4, 0.8, 1]
    alphas.extend(list(np.around(np.linspace(0.0001, 0.001, 10), decimals=4)))
    param_grid = {
        'alpha': alphas,
        'l1_ratio': [0.08, 0.1, 0.3],
        'max_iter': [100000]
    }

    return make_grid(model, train, param_grid)


def xgb_approach(train, valid=None):
    # this function is numpy-dependent
    print('XGBRegressor Approach')

    model = xgb.XGBRegressor()

    param_grid = {
        'objective': ['reg:squarederror'],
        'random_state': [RNG_SEED],
        'n_jobs': [-1],
        'min_child_weight': [1.78],
        'max_depth': [3],
        'silent': [True],
        'n_estimators': [40],
        'learning_rate': [0.05, 0.75],
        'gamma': [0.04, 0.045, 0.05, 0.6],
        'reg_alpha': [0.04, 0.045, 0.05, 0.6],
        'reg_lambda': [0.75, 0.85, 0.90]
    }

    return make_grid(model, train, param_grid)


def lgb_approach(train, valid=None):
    print('LGBMRegressor Approach')

    model = lgb.LGBMRegressor()
    param_grid = {
        'objective': ['regression'],
        'random_state': [RNG_SEED],
        'n_jobs': [-1],
        'min_child_weight': [1.78],
        'bagging_freq': [5],
        'feature_fraction_seed': [9],
        'bagging_seed': [9],
        'min_data_in_leaf': [6],
        'min_sum_hessian_in_leaf': [11],
        'max_bin': [55, 100],
        'n_estimators': [40],
        'learning_rate': [0.05, 0.075],
        'num_leaves': [5, 8, 12],
        'feature_fraction': [0.2, 0.225, 0.25, 0.3],
        'silent': [True],
    }

    return make_grid(model, train, param_grid)


def huber_regressor_approach(train):
    print('HuberRegressor Approach')

    model = HuberRegressor()
    alphas = [0.1, 0.04, 0.0007, 0.0009]  # , 0.1, 0.4, 0.8, 1]
    # alphas.extend(list(np.around(np.linspace(0.0001, 0.001, 10), decimals=4)))
    epsilons = np.linspace(1, 2, 10)
    param_grid = {
        'alpha': np.around(np.linspace(0.01, 0.1, 10), decimals=4),
        "epsilon": [1],
    }
    # DO NOT SET THIS PARAMETER
    # 'max_iter': [10000],
    return make_grid(model, train, param_grid)


def averaging_models_approach(train, models=None, weights=None, valid=None, test=None):
    print('AveragingModels Approach')

    alphas = [0.0001, 0.0004, 0.0008, 0.001, 0.004, 0.008, 0.01, 0.04, 0.08, 0.1, 0.4, 0.8, 1]
    n_estimators = 300

    """
    KernelRidge=0.13259510934221289
	LassoCV=0.13550000763590225
	ElasticNetCV=0.13559871471062945
	RidgeCV=0.13650148586945293
	LinearRegression=0.13683117255037028
    """

    if models is None:
        models = [  # LinearRegression(normalize=True),
            LassoCV(
                alphas=alphas,
                max_iter=70000,
                cv=10,
                normalize=True
            ),
            ElasticNetCV(
                alphas=alphas,
                l1_ratio=.9,
                random_state=RNG_SEED,
                cv=10,
                normalize=True
            ),
            RidgeCV(
                alphas=alphas,
                normalize=True
            ),
            KernelRidge(
                alpha=0.6, kernel='polynomial', degree=2, coef0=2.5
            ),
        ]

    model = AveragingModels(models=models)
    param_grid = {
        'weights': [[0.51, 0.49], [0.49, 0.51],
                    [0.52, 0.48], [0.48, 0.52],
                    [0.53, 0.47], [0.47, 0.53],
                    [0.54, 0.46], [0.46, 0.54],
                    [0.55, 0.45], [0.45, 0.55],
                    [0.56, 0.44], [0.44, 0.56],
                    [0.57, 0.43], [0.43, 0.57],
                    [0.58, 0.42], [0.42, 0.58],
                    [0.59, 0.41], [0.41, 0.59],
                    ],
    }

    return make_grid(model, train, param_grid)


def stacking_models_approach(train):
    print('StackingModels Approach')

    best = BestModels()

    model = StackingModels(base_models=best.models, meta_model=best.kernel_ridge)
    param_grid = {}

    return make_grid(model, train, param_grid)


'''
Explorer Functions
'''


def model_driven_exploratory_data_analysis(train, valid, test):
    features = set()
    for i in range(5):
        features_to_keep = random_forrest_approach(train, valid, test, ispd=True)
        print(f'\tFeatures To Keep: {features_to_keep}')
        features.update(features_to_keep)
    print(f'Features: {features}')

    # n_esimators = 160
    # Features: {'GarageCars', 'YearBuilt', 'OverallQual', 'TotalBsmtSF', 'GrLivArea', 'ExterQual_3'}

    # n_esimators = 3000
    # Features: {'YearBuilt', 'GrLivArea', 'TotalBsmtSF', 'OverallQual'}
    return features


def temporal_features_exploration(train, valid, test):
    # This function requires that train, valid and test contain pd.DataFrames
    temporal_features = ['YearBuilt', 'YearRemodAdd', 'GarageYrBlt', 'YrSold']
    train.X = train.X[temporal_features]
    valid.X = valid.X[temporal_features]
    random_forrest_approach(train, valid, test, ispd=True)


def make_submission(model, train, test, ids, display=False) -> str:
    print('Making submission.csv...')

    print('Fitting on the entire training set...')
    model.fit(train.X, train.y)

    print('Making predictions...')
    predictions = model.predict(test.X)
    predictions = np.round(np.exp(predictions))
    predictions = predictions.reshape((-1,))

    data = {
        'Id': ids,
        'SalePrice': predictions
    }
    dtypes = {
        'Id': 'int16',
        'SalePrice': 'int32'
    }
    submission = pd.DataFrame(data=data)
    submission = submission.astype(dtypes)
    if display:
        print(submission.head(10))
        print(submission.info(verbose=True))
    submission_path = f'{SERIALIZED_DIR}/submission.csv'
    submission.to_csv(submission_path, index=False)
    return submission_path


'''
Controller Functions
'''


@timer
def main(ispd=True):
    train, test = load_data(serialize=True)
    test_ids = test.X['Id'].copy()
    # lgb_approach(train)

    train, test = load_data_as_numpy(scale=True, pca_analyse=True)

    """ Regular Models """
    # kernel_ridge_approach(train)
    # lasso_approach(train)
    # ridge_approach(train)
    # svr_approach(train)
    # elastic_net_approach(train)
    # xgb_approach(train)
    # lgb_approach(train)
    # huber_regressor_approach(train)
    # random_forrest_approach(train, valid, test, ispd=False)

    """ Ensemble Models """
    # multi_approach(train)

    # models = [kernel_ridge, lasso, ridge, svr, elastic_net, huber_regressor]
    # Training Set    RMSLE_CV   =             0.108126
    # 	Training Set    RMSLE_CV   =            0.1077757 weights=[0.3, 0.02, 0.2, 0.25, 0.03, 0.2])
    # 	Training Set    RMSLE_CV   =            0.1076275 weights=[0.4, 0.02, 0.1, 0.25, 0.03, 0.2])
    # 	Training Set    RMSLE_CV   =            0.1076631 models=[kernel_ridge, svr], weights=[0.4, 0.6])
    # 	Training Set    RMSLE_CV   =            0.1077151 models=[kernel_ridge, svr], weights=[0.6, 0.4])
    # models=[kernel_ridge, svr], {'weights': [0.51, 0.49]}    0.107839610308491

    # models = best_models()
    # kernel_ridge = models[0]
    # svr = models[3]
    # averaging_models_approach(train, models=[kernel_ridge, svr])

    # 0     {}         mean:0.108712         std:0.00136
    # model: StackingModels = stacking_models_approach(train)
    best = BestModels()
    model = StackingModels(base_models=best.models, meta_model=best.kernel_ridge)
    model.fit(train.X, train.y)
    X_train_stack, X_test_stack = model.get_oof(X=train.X, y=train.y, test_X=test.X)
    X_train_add = np.hstack((train.X, X_train_stack))
    X_test_add = np.hstack((test.X, X_test_stack))

    train.X = X_train_add
    test.X = X_test_add
    print(train.X.shape, test.X.shape)

    # model = stacking_models_approach(train)
    """  params  mean_test_score  std_test_score
        0     {}         0.102753        0.001091"""

    """ Deep Learning Models """
    # nn_start_small_approach(train)
    rf = RandomForestRegressor(
        n_estimators=3000,
        min_samples_leaf=3,
        max_features=0.5,
        oob_score=True)
    rf.fit(train.X, train.y)
    rmsle_cv(rf, train)

    create_submission = False
    if create_submission:
        make_submission(model, train, test, test_ids)


if __name__ == '__main__':
    main(ispd=True)
