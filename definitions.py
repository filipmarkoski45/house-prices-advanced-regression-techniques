import datetime
import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
DATA_DIR = os.path.join(ROOT_DIR, 'data')
DOCS_DIR = os.path.join(ROOT_DIR, 'docs')
TESTS_DIR = os.path.join(ROOT_DIR, 'tests')
PLOTS_DIR = os.path.join(ROOT_DIR, 'plots')
SERIALIZED_DIR = os.path.join(ROOT_DIR, 'serialized')
MODELS_DIR = os.path.join(SERIALIZED_DIR, 'models')
RNG_SEED = 1

training_set_file_path = f'{DATA_DIR}/train.csv'
testing_set_file_path = f'{DATA_DIR}/test.csv'

serialized_feather = f'{SERIALIZED_DIR}/data.feather'
serialized_train_feather = f'{SERIALIZED_DIR}/train.feather'
serialized_y_train_feather = f'{SERIALIZED_DIR}/y_train.feather'
serialized_valid_feather = f'{SERIALIZED_DIR}/valid.feather'
serialized_y_valid_feather = f'{SERIALIZED_DIR}/y_valid.feather'
serialized_X_test_feather = f'{SERIALIZED_DIR}/X_test.feather'
serialized_y_test_feather = f'{SERIALIZED_DIR}/y_test.feather'

serialized_data_sets = [serialized_train_feather,
                        serialized_valid_feather,
                        serialized_X_test_feather]


def timer(func):
    def f(*args, **kwargs):
        start = datetime.datetime.now()
        timestamp = '{date:%Y-%m-%d %H:%M:%S}'.format(date=start)
        print(f'Execution started: {timestamp}')
        rv = func(*args, **kwargs)
        end = datetime.datetime.now()
        timestamp = '{date:%Y-%m-%d %H:%M:%S}'.format(date=end)
        print(f'Execution ended: {timestamp}')
        print('Execution elapsed: ', end - start)
        return rv

    return f


# features selected by the most voted kernel, the EDA kernel
eda_features = ['OverallQual', 'GrLivArea', 'GarageCars', 'TotalBsmtSF', 'FullBath', 'YearBuilt']

# features with importance > 0
above_zero_features = ['GrLivArea', 'OverallQual', 'TotalBsmtSF', 'YearBuilt', '1stFlrSF', 'GarageCars', 'LotArea',
                       'BsmtFinSF1', 'FullBath', 'Fireplaces', 'GarageArea', 'ExterQual_2', 'ExterQual_3',
                       'CentralAir_1',
                       'LotFrontage', 'OverallCond', 'YearRemodAdd', 'BsmtUnfSF', '2ndFlrSF', 'GarageYrBlt',
                       'FireplaceQu_3', 'BsmtUnfSF', 'KitchenQual_3', '1stFlrSF', 'BedroomAbvGr', 'Fireplaces']

'''
fill_missing_values deletes the 'Utilities' column
'''
houses_dtypes = {
    'Id': 'int16',
    'MSSubClass': 'int8',  # categorical
    'MSZoning': 'str',
    'LotFrontage': 'float32',
    'LotArea': 'float32',
    'Street': 'str',
    'Alley': 'str',
    'LotShape': 'str',
    'LandContour': 'str',
    'Utilities': 'str',
    'LotConfig': 'str',
    'LandSlope': 'str',
    'Neighborhood': 'str',
    'Condition1': 'str',
    'Condition2': 'str',
    'BldgType': 'str',
    'HouseStyle': 'str',
    'OverallQual': 'int8',
    'OverallCond': 'int8',
    'YearBuilt': 'int16',
    'YearRemodAdd': 'int16',
    'RoofStyle': 'str',
    'RoofMatl': 'str',
    'Exterior1st': 'str',
    'Exterior2nd': 'str',
    'MasVnrType': 'str',
    'MasVnrArea': 'int16',  # 'float32',  # int16
    'ExterQual': 'str',
    'ExterCond': 'str',
    'Foundation': 'str',
    'BsmtQual': 'str',
    'BsmtCond': 'str',
    'BsmtExposure': 'str',
    'BsmtFinType1': 'str',
    'BsmtFinSF1': 'int16',  # int16
    'BsmtFinType2': 'str',
    'BsmtFinSF2': 'int16',  # int16
    'BsmtUnfSF': 'int16',  # int16
    'TotalBsmtSF': 'int16',  # int16
    'Heating': 'str',
    'HeatingQC': 'str',
    'CentralAir': 'str',
    'Electrical': 'str',
    '1stFlrSF': 'int16',
    '2ndFlrSF': 'int16',
    'LowQualFinSF': 'int16',
    'GrLivArea': 'int16',
    'BsmtFullBath': 'int8',  # int8
    'BsmtHalfBath': 'int8',  # int8
    'FullBath': 'int8',
    'HalfBath': 'int8',
    'BedroomAbvGr': 'int8',
    'KitchenAbvGr': 'int8',
    'KitchenQual': 'str',
    'TotRmsAbvGrd': 'int8',
    'Functional': 'str',
    'Fireplaces': 'int8',
    'FireplaceQu': 'str',
    'GarageType': 'str',
    'GarageYrBlt': 'int16',  # int16
    'GarageFinish': 'str',
    'GarageCars': 'int8',  # int8
    'GarageArea': 'int16',  # int16
    'GarageQual': 'str',
    'GarageCond': 'str',
    'PavedDrive': 'str',
    'WoodDeckSF': 'int16',
    'OpenPorchSF': 'int16',
    'EnclosedPorch': 'int16',
    '3SsnPorch': 'int16',
    'ScreenPorch': 'int16',
    'PoolArea': 'int8',  # absolutely useless, only-zero column
    'PoolQC': 'str',  # complete NA column, NA stands for 'No pool'
    'Fence': 'str',
    'MiscFeature': 'str',
    'MiscVal': 'int16',
    'MoSold': 'int8',  # month sold, 1 - 12
    'YrSold': 'int16',
    'SaleType': 'str',  # categorical
    'SaleCondition': 'str',
    'SalePrice': 'float32'  # int32, but float because floats can have null
}

x_houses_dtypes = {
    'Id': 'int16',
    'MSSubClass': 'int8',  # categorical
    'MSZoning': 'str',
    'LotFrontage': 'float32',
    'LotArea': 'float32',
    'Street': 'str',
    'Alley': 'str',
    'LotShape': 'str',
    'LandContour': 'str',
    'Utilities': 'str',
    'LotConfig': 'str',
    'LandSlope': 'str',
    'Neighborhood': 'str',
    'Condition1': 'str',
    'Condition2': 'str',
    'BldgType': 'str',
    'HouseStyle': 'str',
    'OverallQual': 'int8',
    'OverallCond': 'int8',
    'YearBuilt': 'int16',
    'YearRemodAdd': 'int16',
    'RoofStyle': 'str',
    'RoofMatl': 'str',
    'Exterior1st': 'str',
    'Exterior2nd': 'str',
    'MasVnrType': 'str',
    'MasVnrArea': 'int16',  # 'float32',  # int16
    'ExterQual': 'str',
    'ExterCond': 'str',
    'Foundation': 'str',
    'BsmtQual': 'str',
    'BsmtCond': 'str',
    'BsmtExposure': 'str',
    'BsmtFinType1': 'str',
    'BsmtFinSF1': 'Int16',  # int16
    'BsmtFinType2': 'str',
    'BsmtFinSF2': 'Int16',  # int16
    'BsmtUnfSF': 'Int16',  # int16
    'TotalBsmtSF': 'Int16',  # int16
    'Heating': 'str',
    'HeatingQC': 'str',
    'CentralAir': 'str',
    'Electrical': 'str',
    '1stFlrSF': 'int16',
    '2ndFlrSF': 'int16',
    'LowQualFinSF': 'int16',
    'GrLivArea': 'int16',
    'BsmtFullBath': 'Int8',  # int8
    'BsmtHalfBath': 'Int8',  # int8
    'FullBath': 'int8',
    'HalfBath': 'int8',
    'BedroomAbvGr': 'int8',
    'KitchenAbvGr': 'int8',
    'KitchenQual': 'str',
    'TotRmsAbvGrd': 'int8',
    'Functional': 'str',
    'Fireplaces': 'int8',
    'FireplaceQu': 'str',
    'GarageType': 'str',
    'GarageYrBlt': 'Int16',  # int16
    'GarageFinish': 'str',
    'GarageCars': 'Int8',  # int8
    'GarageArea': 'Int16',  # int16
    'GarageQual': 'str',
    'GarageCond': 'str',
    'PavedDrive': 'str',
    'WoodDeckSF': 'int16',
    'OpenPorchSF': 'int16',
    'EnclosedPorch': 'int16',
    '3SsnPorch': 'int16',
    'ScreenPorch': 'int16',
    'PoolArea': 'int8',  # absolutely useless, only-zero column
    'PoolQC': 'str',  # complete NA column, NA stands for 'No pool'
    'Fence': 'str',
    'MiscFeature': 'str',
    'MiscVal': 'int16',
    'MoSold': 'int8',  # month sold, 1 - 12
    'YrSold': 'int16',
    'SaleType': 'str',  # categorical
    'SaleCondition': 'str',
    'SalePrice': 'int32'  # int32
}
