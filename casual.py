import numpy as np
import pandas as pd

from metrics import timestamp


def list_difference(one: list, two: list) -> list:
    return list(set(one).symmetric_difference(set(two)))


def pandas_split_X_y_example():
    a = np.array([1, 2, 3])
    b = np.array([4, 5, 6])

    train = pd.DataFrame()

    train['X'] = a
    train['y'] = b

    print(train.y)


def pandas_isna_column_dummies_example():
    a = [True, False, True, False, False]
    b = ['yes', 'no', 'yes', 'no', 'no']
    frames = [pd.DataFrame(data=a), pd.DataFrame(data=b)]
    for df in frames:
        print(df.head())
        d = pd.get_dummies(df)
        print(d.head())
        print(d.dtypes)


def np_arange_linspace():
    print(np.around(np.linspace(1, 15, 10), decimals=2))
    print(np.arange(0, 1, 0.2))

    alphas = [0.01, 0.04, 0.08, 0.1, 0.4, 0.8, 1]
    alphas.extend(list(np.around(np.linspace(0.0001, 0.001, 10), decimals=4)))
    print(type(alphas))

    models = [pd.DataFrame()] * 4
    n = len(models)
    weights = [1 / n] * n
    print(weights)


def main():
    print(timestamp())


if __name__ == '__main__':
    main()
