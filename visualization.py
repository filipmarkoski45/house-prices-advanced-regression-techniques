import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
from scipy.stats import norm

from definitions import PLOTS_DIR
from preprocess_data import load_data


def hist_SalePrice(df_train):
    sns.distplot(df_train['SalePrice'], color='red')
    plt.savefig('{dir}/hist_SalePrice.png'.format(dir=PLOTS_DIR))


def scatter_plot(train, feature, class_feature='SalePrice'):
    data = pd.concat([train[class_feature], train[feature]], axis=1)
    data.plot.scatter(x=feature, y='SalePrice', ylim=(0, 800000))

    x = sorted(train[feature].to_numpy())
    y = sorted(train[class_feature].to_numpy())
    plt.plot(np.unique(x), np.poly1d(np.polyfit(x, y, 1))(np.unique(x)), color='red')


def scatter_column(df_train, column):
    data = pd.concat([df_train['SalePrice'], df_train[column]], axis=1)
    data.plot.scatter(x=column, y='SalePrice', ylim=(0, 800000))

    x = sorted(df_train[column].to_numpy())
    y = sorted(df_train['SalePrice'].to_numpy())
    plt.plot(np.unique(x), np.poly1d(np.polyfit(x, y, 1))(np.unique(x)), color='red')
    plt.savefig("{dir}/scatter_x_{x}_and_y_{y}.png"
                .format(dir=PLOTS_DIR, x=column, y="SalePrice"))


def boxplot_column(df_train, column="OverallQual"):
    data = pd.concat([df_train['SalePrice'], df_train[column]], axis=1)
    f, ax = plt.subplots(figsize=(8, 6))
    fig = sns.boxplot(x=column, y="SalePrice", data=data)
    fig.axis(ymin=0, ymax=800000)
    plt.savefig("{dir}/boxplot_x_{x}_and_y_{y}.png"
                .format(dir=PLOTS_DIR, x=column, y="SalePrice"))


def boxplot_YearBuilt(df_train, column="YearBuilt"):
    data = pd.concat([df_train['SalePrice'], df_train[column]], axis=1)
    f, ax = plt.subplots(figsize=(16, 8))
    fig = sns.boxplot(x=column, y="SalePrice", data=data)
    fig.axis(ymin=0, ymax=800000)
    plt.xticks(rotation=90)
    plt.savefig("{dir}/boxplot_x_{x}_and_y_{y}.png"
                .format(dir=PLOTS_DIR, x=column, y="SalePrice"))


def heatmap(df_train):
    correlation_matrix = df_train.corr()
    f, ax = plt.subplots(figsize=(12, 9))
    sns.heatmap(correlation_matrix, vmax=.8, square=True)
    plt.savefig("{dir}/heatmap.png"
                .format(dir=PLOTS_DIR))


def heatmap_SalePrice(df_train):
    correlation_matrix = df_train.corr()
    k = 10  # number of variables for heatmap
    cols = correlation_matrix.nlargest(k, 'SalePrice')['SalePrice'].index
    cm = np.corrcoef(df_train[cols].values.T)
    sns.set(font_scale=1.25)
    hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f',
                     annot_kws={'size': 10}, yticklabels=cols.values,
                     xticklabels=cols.values)
    plt.savefig("{dir}/heatmap_SalePrice.png"
                .format(dir=PLOTS_DIR))


def scatter_matrix(df_train):
    sns.set()
    cols = ['SalePrice', 'OverallQual', 'GrLivArea', 'GarageCars',
            'TotalBsmtSF', 'FullBath', 'YearBuilt']
    sns.pairplot(df_train[cols], size=2.5)
    plt.savefig("{dir}/scatter_matrix.png"
                .format(dir=PLOTS_DIR))


'''
Note: This is the final version of this function
'''


def qq_plot(df_train, column='SalePrice', log_transform=True):
    if log_transform:
        df_train[column] = np.log(df_train[column])
    sns.distplot(df_train[column], fit=norm, color="red")
    plt.savefig("{dir}/hist_{column}_{logd}.png"
                .format(dir=PLOTS_DIR, column=column, logd=log_transform))
    fig = plt.figure()
    stats.probplot(df_train[column], plot=plt)
    plt.savefig("{dir}/qq_{column}_{logd}.png"
                .format(dir=PLOTS_DIR, column=column, logd=log_transform))


def execute():
    df_train = pd.read_csv("data/train.csv")
    print(df_train['SalePrice'].describe())

    # hist_SalePrice(df_train)
    print("Skewness: {skew}".format(skew=df_train['SalePrice'].skew()))
    print("Kurtosis: {kurt}".format(kurt=df_train['SalePrice'].kurt()))

    '''
    [GrLivArea]: Shows a positive linear relationship
    '''
    # scatter_column(df_train, 'GrLivArea')
    '''
    [TotalBsmtSF]: Shows a positive linear relationship 
    even higher than that of GrLivArea's
    '''
    # scatter_column(df_train, 'TotalBsmtSF')

    '''
    [OverallQual]: There's definitely a relationship
    '''
    # boxplot_column(df_train, 'OverallQual')

    '''
    SalePrice has gone up in recent years
    - it's probably inflation after the 
    - 2008 housing crisis when the economy collapsed
    '''
    # boxplot_YearBuilt(df_train)

    boxplot_column(df_train, 'Street')
    boxplot_column(df_train, 'Neighborhood')

    # heatmap(df_train)

    # heatmap_SalePrice(df_train)
    # scatter_matrix(df_train)

    # qq_plot(df_train, log_transform=False)
    print(df_train.head())
    plt.show()


def main():
    df_train = pd.read_csv("data/train.csv")

    X_train, y_train, X_valid, y_valid, X_test, y_test = load_data()
    sns.distplot(df_train['SalePrice'],
                 kde_kws={"color": "coral", "lw": 1, "label": "KDE"},
                 hist_kws={"histtype": "stepfilled", "linewidth": 3, "alpha": 1, "color": "skyblue"})

    fig = plt.figure()
    res = stats.probplot(df_train['SalePrice'], plot=plt)

    # plt.show()


if __name__ == '__main__':
    main()
