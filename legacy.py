import pandas as pd
from scipy.special import boxcox1p
from scipy.stats import boxcox_normmax
from scipy.stats import skew
from sklearn.feature_selection import SelectFromModel
from sklearn.preprocessing import LabelEncoder

# Local Imports
from definitions import houses_dtypes
from pd_utils import missing_data_ratio


def change_dtypes(df: pd.DataFrame, dtypes: dict, class_feature: str):
    if not class_feature in df.columns:
        del dtypes[class_feature]
    df = df.astype(dtypes)
    return df


def fill_missing_values(data: pd.DataFrame):
    # https://www.kaggle.com/jolasa/eda-anda-data-preparation-for-modelling-top-1\
    # https://www.kaggle.com/serigne/stacked-regressions-top-4-on-leaderboard
    features_fill_na_none = ['PoolQC', 'MiscFeature', 'Alley', 'Fence', 'FireplaceQu',
                             'GarageQual', 'GarageCond', 'GarageFinish', 'GarageType',
                             'BsmtExposure', 'BsmtCond', 'BsmtQual', 'BsmtFinType1', 'BsmtFinType2',
                             'MasVnrType']

    for feature_none in features_fill_na_none:
        data[feature_none].fillna('None', inplace=True)

    features_fill_na_0 = ['GarageYrBlt', 'GarageCars', 'GarageArea', 'MasVnrArea',
                          'BsmtFullBath', 'BsmtHalfBath', 'BsmtFinSF1', 'BsmtFinSF2',
                          'BsmtUnfSF', 'TotalBsmtSF']

    for feature_0 in features_fill_na_0:
        data[feature_0].fillna(0, inplace=True)

    # LotFrontage
    # We'll fill missing values by the median of observation's Neighborhood
    data["LotFrontage"] = data.groupby("Neighborhood")["LotFrontage"].transform(
        lambda x: x.fillna(x.median()))

    # MSzoning - 4 missing values
    # We'll fill missing values with most common value
    data['MSZoning'] = data['MSZoning'].fillna(data['MSZoning'].mode()[0])

    # Utilities
    # All records have "AllPub", but 3. From those 3, 2 are NA and one is "NoSeWa" is
    # in the training set.
    # We may proceed to drop this column because it has no variation
    # data = data.drop(columns=['Utilities'], axis=1)

    # Functional: NA means "Typ"
    data["Functional"] = data["Functional"].fillna("Typ")

    # Electrical - 91% of observations have Electrical = SBrkr
    # We'll fill missing values with SBrkr
    data['Electrical'] = data['Electrical'].fillna("SBrkr")

    # Exterior1st and Exterior2nd, one missing value, same observation
    # We'll substitute it with the most common value
    data['Exterior1st'] = data['Exterior1st'].fillna(data['Exterior1st'].mode()[0])
    data['Exterior2nd'] = data['Exterior2nd'].fillna(data['Exterior2nd'].mode()[0])

    # KitchenQual, ony one missing value
    # We'll substitute it with the most common value
    data['KitchenQual'] = data['KitchenQual'].fillna(data['KitchenQual'].mode()[0])

    # Saletype, ony one missing value
    # We'll subsitute it with the most common value
    data['SaleType'] = data['SaleType'].fillna(data['SaleType'].mode()[0])
    return data


def get_numerical_features(df: pd.DataFrame) -> list():
    # returns a list of names of the features that are numerical
    numerical_features = df.dtypes[df.dtypes != "object"].index
    return numerical_features


def get_categorical_features(df: pd.DataFrame) -> list():
    # returns a list of names of the features that are categorical
    categorical_features = df.dtypes[df.dtypes == "object"].index
    return categorical_features


def normalize_numerical_features(data: pd.DataFrame):
    print('Standardizing numerical features...')
    numerical_features = get_numerical_features(data)
    categorical_features = get_categorical_features(data)
    print("We have: ", len(numerical_features), 'Numerical Features')
    print("We have: ", len(categorical_features), 'Categorical Features')
    skewed_features = data[numerical_features].apply(lambda x: skew(x)).sort_values(ascending=False)

    norm_target_features = skewed_features[skewed_features > 0.5]
    norm_target_index = norm_target_features.index
    print("#{} numerical features need normalization; :".format(norm_target_features.shape[0]))
    skewness = pd.DataFrame({'Skew': norm_target_features})

    # Normalizing with Box Cox Transformation
    for i in norm_target_index:
        data[i] = boxcox1p(data[i], boxcox_normmax(data[i] + 1))

    return data


def encode_categorical_variables(data: pd.DataFrame):
    print('Encoding categorical variables...')
    # Let's encode categorical variables
    encode_cat_variables = ('FireplaceQu', 'BsmtQual', 'BsmtCond', 'GarageQual', 'GarageCond',
                            'ExterQual', 'ExterCond', 'HeatingQC', 'PoolQC', 'KitchenQual', 'BsmtFinType1',
                            'BsmtFinType2', 'Functional', 'Fence', 'BsmtExposure', 'GarageFinish', 'LandSlope',
                            'LotShape', 'PavedDrive', 'Street', 'Alley', 'CentralAir', 'MSSubClass', 'OverallCond',
                            'YrSold', 'MoSold')
    categorical_features = get_categorical_features(data)
    for variable in categorical_features:
        label_encoder = LabelEncoder()
        label_encoder.fit(list(data[variable].values))
        data[variable] = label_encoder.transform(list(data[variable].values))

    return data


def preprocess_data(data: pd.DataFrame, class_feature: str) -> pd.DataFrame:
    missing_data = missing_data_ratio(data)
    features_with_na = missing_data[missing_data['Total'] > 1].T.columns.to_list()
    print(features_with_na)
    # features_with_na.remove('Utilities')

    # data = append_isna_columns(df=data, columns=features_with_na)
    data = fill_missing_values(data)
    data = normalize_numerical_features(data)
    data = encode_categorical_variables(data)
    data = change_dtypes(data, houses_dtypes, class_feature)

    data = data.drop((missing_data[missing_data['Total'] > 1]).index, 1)
    data = pd.get_dummies(data, drop_first=True)

    data.reset_index(drop=True, inplace=True)
    # data.to_csv(f'{SERIALIZED_DIR}/data.csv')

    return data


def _(model, train, valid):
    feature_selection = SelectFromModel(model, threshold=0.15)
    feature_selection.fit(train.X, train.y)
    selected_columns = train.X.columns[feature_selection.get_support()]
    X_train = feature_selection.transform(train.X)
    print("Shape of new data:", X_train.shape)
    print("Selected columns:", selected_columns)
    X_test_new = feature_selection.transform(train.X)


def _from_data_clean(full, train_ids, test_ids):
    # Splitting the concatinated data set composed of train and test

    X_train = full.loc[full['Id'].isin(train_ids)].copy()
    X_test = full.loc[full['Id'].isin(test_ids)].copy()

    features_to_encode = ['YearBuilt', 'YearRemodAdd', 'GarageYrBlt']
    X_train, X_test = encode_features(X_train, X_test, features_to_encode)

    # Re-concatanating the train and test sets

    full = pd.concat((X_train, X_test), ignore_index=True, sort=False)
    return full


def encode_features(X_train, X_test, features, encoder=LabelEncoder()):
    for feature in features:
        encoder = LabelEncoder()
        X_train[feature] = encoder.fit_transform(X_train[feature])
        X_test[feature] = encoder.transform(X_test[feature])
    return X_train, X_test


def main():
    pass


if __name__ == '__main__':
    main()
