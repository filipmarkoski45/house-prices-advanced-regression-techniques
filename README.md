# House Prices: Advanced Regression Techniques

`kaggle` source: https://www.kaggle.com/c/house-prices-advanced-regression-techniques/

- A project for my data mining class at FCSE (a.k.a. FINKI)
- Made by *Filip Markoski, 161528, KNI-A*

## This project is **in the top 16%** out of 4359 contestants:

![alt](docs/images/kaggle_houses_leaderboard_detail.png)
![alt](docs/images/kaggle_houses_leaderboard.png)

# What I've learned

## Various Concepts and Models briefly explained

### Regularization

This is a form of regression, that constrains, regularizes or shrinks the coefficient estimates towards zero. In other words, this technique discourages learning a more complex or flexible model, so as to avoid the risk of overfitting.

Overfitting occurs when a model tries to predict a trend in data that is too noisy, i.e. with too much residual variation. Overfitting is the result of an overly complex model with too many parameters. A model that is overfitted is inaccurate because the trend does not reflect the reality of the data.

![regularization](docs/images/overfit.png)

One of the ways of avoiding overfitting is using **cross-validation** which helps in estimating the error over a validation set, and in deciding which parameters work best for the model.

Additionally, **early stopping** can be viewed as regularization in time. By regularizing on time, the complexity of the model can be controlled, improving generalization.

The model is trained until performance on the validation set no longer improves. 

The `early_stopping_rounds` parameter specifies a window of the number of epochs over which no improvement is observed.

```python
xgb.XGBRegressor().fit(train.X, train.y, early_stopping_rounds=10, eval_set=eval_set)
```

### Ridge Regression

![ridge](docs/images/ridge.png)

In Ridge Regression the residual sum of squares or RSS is modified by adding lambda (λ) which is the tuning parameter that decides how much we want to penalize the flexibility of our model

When `λ` equals zero, the penalty term has no eﬀect, and the estimates produced by ridge regression will be equal to least squares. However, as `λ` approaches infinity, the impact of the shrinkage penalty grows, and the ridge regression coeﬃcient estimates will approach zero

The coefficient estimates produced by this method are also known as the `L`<sub>`2`</sub> norm. They are not produced by the standard least squares method which means that they are not scale equivariant, which means that scaling the values does affect the prediction. Thus, ridge regression needs its predictors to be standardized, i.e. scaled.

### Lasso Regression

Lasso Regression is very similar to Ridge Regression with the only difference being the way the high coefficients are being penalized.

It uses `|βj|` (modulus) instead of `β`<sup>`2`</sup> as its penalty. In statistics, this is known as the `L`<sub>`1`</sub> norm.

By looking at the penalty part of the Ridge formula we can see that we can modify it simply by changing the value of `p` in the `L`<sub>`p`</sub> regularizer shown in the figure.

![ridge](docs/images/ridge_2.png)

![ps](docs/images/ps.png)

There is a balance that must be met between the minimization of the data term and the adjustment of the regularization term `lambda` and graphically that can be represented as the intersection between the two which is ideal, a combination of both that minimizes the overall rate of error.

![regul](docs/images/regul.png)


Since Ridge Regression has a circular constraint with no sharp points, this intersection will not generally occur on an axis, and so the ridge regression coeﬃcient estimates will be exclusively non-zero. However, the lasso constraint has corners at each of the axes, and so the ellipse will often intersect the constraint region at an axis. When this occurs, one of the coeﬃcients will equal zero.

When it comes to computational effficiency, L1-norm does not have an analytical solution, but L2-norm does. This allows the L2-norm solutions to be calculated computationally efficiently. However, L1-norm solutions does have the sparsity properties which allows it to be used along with sparse algorithms, which makes the calculation more computationally efficient.

### ElasticNet Regression

ElasticNet Regression penalizes the size of the regression coefficients based on both their `L`<sub>`1`</sub> norm and `L`<sub>`2`</sub> norm.

![elasticNet](docs/images/el_net_form.png)

The `L`<sub>`1`</sub> norm penalty generates a sparse model.

The `L`<sub>`2`</sub> norm penalty removes the limitation on the number of selected variables, encourages a grouping effect and stabilizes the `L`<sub>`1`</sub> regularization path.

![elasticNet](docs/images/L1-L2-elasticNet.png)

### Kernelization (The Kernel Trick)

This concept can be found while learning about Support Vector Machines.

![alt](docs/images/kernel.jpg)

* `l` is the number of data points in our training data. 
* `y`’s denote the outputs of the data points, which for our convenience, we express as +1 or -1. 
* `x` is the feature vector in each training example. 
* `alpha` is the Lagrangian constant.

While minimizing the function `W(alpha)`, i.e. the weight vector as a function of alpha, we see the term `x * x`<sup>`T`</sup> which means that we don't need the exact data points, but only their inner products to compute the decision boundry (in the case of SVMs)

![k_nk](docs/images/k_nk.png)

THis implies that if we want transform our existing data into a higher dimensional data, we needn't compute the exact transformation of our data, we just need the inner product of our data in that higher dimensional space, which is a lot easier to compute. 

![svm](docs/images/svm.png)

For a more mathematically-detailed run-down go to this [link](https://youtu.be/wBVSbVktLIY)

### Batch Normalization

Batch normalization is a regularization technique for neural network, similary as adding a drop-out layer.

Batch normalization reduces the amount by which the hidden unit values shift around (covariance shift), in essence, batch normalization helps the neural network keep its generalized predictive power and prevents it from overfitting. 

In my case, adding a batch normalization layer after every hidden layer helped the neural network model perform better.

The recipe for batch normalization is the following:
1. Normalize the output from the activation function using standardization, i.e. subtract the mean and divide by the standard deviation.

    `z = (x - m) / s`
2. Multiply the normalized output by the arbitrary parameter `g`.

    `z * g`
3. Add the arbitrary parameter `b` to the resulting product.

    `(z * g) + b`

### Neural Networks

1. If the weights and biases change so does the neuron's value.

### Part 0

1. NaN is a float, e.g. `np.nan`
2. `np.int64` and `pd.Int64Dtype()` are not equivalent. The latter permits `NA` values
3. `pd.isnull()` is an alias for `pd.isna()`
4. Changing the `dtypes` of a `pd.DataFrame` might augment the value. It might even make the value negative. Usually this happens because of a conversion from signed to unsigned.
5. To install a specific version of a pip package do something similar to 
    ```python
    pip install xgboost==0.82
    ```
6. `xgboost` requires a `pandas` version of less than `23.0` because the `Series.base` variable has been depricated. Additionally, the makers have noted that unexpected errors may occur on Windows, and they do. Thus, `xgboost` is not a viable option. <br>
To overcome installing `xgboost` on `Windows 10 x64` go to <br>
https://medium.com/@rakshithvasudev/how-i-installed-xgboost-after-a-lot-of-hassels-on-my-windows-machine-c53e972e801e <br> 
Download the `.whl` from here: <br>
https://www.lfd.uci.edu/~gohlke/pythonlibs/
7. The `n_jobs=-1` parameter relies heavily on `python`'s built-in multiprocess management. It often fails due to an error and it's best left unset. Or at least try it on a single model before grid searching. Also, `n_jobs=-1` on `GridSearchCV` has the same problem.
8. Do not set `max_iter` on `HuberRegressor`!

### Part 1

1. You should **always** specify the data types of your data to aid `pandas` in creating a better `pd.DataFrame()`. The `low_memory` option should be depricated. The setting `low_memory = False`,  forces it to read more of the file to decide what the types are.
    ```python
    pd.read_csv(path, dtype={"user_id": int, "username": object})
    ```
2. The variable we want to predict is called the *Dependent Variable* in this case our dependent variable is `SalePrice`
3. RMSLE or root mean squared log error. The reason we use log is because, generally, you don't care as much about missing by $10 but missing by 10%. 
4. XGBoost is better than Gradient Boosting Tree
5. `pip install feather-format`.  It's better than `pickle`
    ```python
    df_feather_path = f'{SERIALIZED_DIR}\df.feather'

    df = pd.DataFrame()
    df.to_feather(df_feather_path)
    df = pd.read_feather(df_feather_path)
    ```
6.  `n_jobs = -1` tells the `RandomForestRegressor` to create a separate job/process for each CPU you have.

### Part 2

1. Hyper-parameters are for example how many estimators are we going to choose. The validation set is mainly use to tune these hyper-parameters.
2. ```python
    RandomForestRegressor(
        n_estimators=40, 
        min_samples_leaf=3,  
        max_features=0.5, 
        n_jobs=-1, # use all the cores you can
        oob_score=True)
    ```
3. *grid search*

### Part 3

1. ```python
    df = pd.read_csv(f'data.csv', 
        parse_dates = ['date'],
        dtype=types, # dict()
        infer_datetime_format=True)
    ```

### Part 4

1. All `oob_score=True` does is it says whatever your subsample is (it might be a bootstrap sample or a subsample), take all of the other rows (for each tree), put them into a different data set, and calculate the error on those. So it doesn’t actually impact training at all. It just gives you an additional metric which is the OOB error. So if you don’t have a validation set, then this allows you to get kind of a quasi validation set for free.
2. The result of increasing `min_samples_leaf` is that now each of our leaf nodes has more than one thing in, so we are going to get a more stable average that we are calculating in each tree. We have a little less depth (i.e. we have less decisions to make) and we have a smaller number of leaf nodes. So, we would expect the result of that node would be that each estimator would be less predictive, but the estimators would be also less correlated. This might help us **avoid overfitting**.
3. With `max_features=0.5` at each split, we pick a different half of the features, this makes the trees to be as rich as possible, i.e. each specializing on a different set of features. Each individual tree is going to be less accurate but the trees are going to be more varied. The default is `max_features=None` which is good for a low number of trees, but if you're using a high number of trees try using `max_features=sqrt` or `log2`. This will make it so the many trees will have different initial splitting points.
4. Remove features with `importance` less than `0.05` and then train the model again and see if there was a decrease in `R`<sup>2</sup>, if there wasn't a degradation in performance then the removal of those unimportant features was beneficial to code comprehension and computational performance.
5. Linear regression model coefficients are much less flexible than random forest coefficients when it comes to feature importance.
6. Agglomerated clustering, also known as, hierarchical clustering can be used to in feature importance. Cluster analysis using a **dendrogram**
    ```python
    from scipy.cluster import hierarchy as hc
    
    corr = np.round(scipy.stats.spearmanr(df).correlation, 4)
    corr_condensed = hc.distance.squareform(1-corr)
    z = hc.linkage(corr_condensed, method='average')
    fig = plt.figure(figsize=(16,10))
    dendrogram = hc.dendrogram(z, labels=df_keep.columns, 
        orientation='left', leaf_font_size=16)
    plt.show()
    ```
7. [Spearman's rank correlation coefficient](https://en.wikipedia.org/wiki/Spearman%27s_rank_correlation_coefficient) is a correlation between two variables instead of a variable and its predictions as `R`<sup>2</sup> is
8. Rank correlation is better because random forests do not care about linearity, they just care about ordering. [Spearman's rank correlation coefficient](https://en.wikipedia.org/wiki/Spearman%27s_rank_correlation_coefficient) is the most common rank correlation.
9. The **partial dependence plot** (short PDP or PD plot) shows the marginal effect one or two features have on the predicted outcome of a machine learning model. A partial dependence plot can show whether the relationship between the target and a feature is linear, monotonic or more complex. In other other words, what's the relationship between `A` and `B` **all other things being equal**, that is, for example,  if we sold something in 1990 vs. 1980 and it was exactly the same thing to exactly the same person in exactly the same auction so on and so forth, what would have been the difference in price?.</br>
Marginal effects tells us how a dependent variable (outcome) changes when a specific independent variable (explanatory variable) changes. **Other covariates are assumed to be held constant.**
    ```python
    from pdpbox import pdp

    def plot_pdp(feat, clusters=None:int, feat_name=None):
        feat_name = feat_name or feat
        p = pdp.pdp_isolate(m, x, feat)
        return pdp.pdp_plot(p, 
            feat_name, 
            plot_lines=True, 
            cluster=clusters is not None,  
            n_cluster_centers=clusters)
    
    plot_pdp('YearMade')

    feats = ['A', 'B']
    p = pdp.pdp_interact(m, x, feats)
    pdp.pdp_interact_plot(p, feats)
    ```
    *ICE plot*

### Part 5

1. the OOB score gives us something which is pretty similar to the validation score, but on average it’s a little less good. Why? Because every row is going to be using a subset of the trees to make its prediction, and with less trees, we know we get a less accurate prediction. Generally, it's close enough.
2. If there is a temporal feature, i.e. a feature related to time, order the training data set by that temporal feature and grab the latest portion to serve as the **validation set**. If the data set is 'timeless' then random sampling should be fine, of course, you should **compare your validation set to your training set** by building multiple models all on different portions of the data (e.g. whole data set, last month, last two weeks etc.) compare the model results or group averages of features of the currently-tested validation set and the actual test set.
3. If you have temporal data there’s no way to do cross validation or no good way to do cross validation. 
4. *Waterfall plots*

## Part 6

1. [**Interaction importance**]: Tree interpreter tells us the contributions for a particular row based on the difference in the tree. We could calculate that for every row in our dataset and add them up. That would tell us feature importance. And it would tell us feature importance in a different way. One way of doing feature importance is by shuffling the columns one at a time. Another way is by doing tree interpreter for every row and adding them up.

## Part 7

1. Magic number `22`, where the t-distribution *sort of* starts to behave like the normal distribution. So, have at least 22 observation for each class in your data set.
2. `one-shot learning` and `zero-shot learning`

## Part 8

1. Area under the ROC curve disregards scale and only cares about sorting, likewise, `RandomForests` are immune to outlier for similar reasons, i.e. because normalization doesn't change the order of the data set, but for deep learning, i.e. neural networks **normalization is a must**.

## Python

[Notebook based off James Powell's talk at PyData 2017'](https://github.com/austin-taylor/code-vault/blob/master/python_expert_notebook.ipynb)
1. ```python
    from dis import dis
    # dis can show python interpreted assembly-like code
    from inspect import getsource, getfile
    ```

## Resources

1. https://www.kaggle.com/dansbecker/partial-dependence-plots/
2. https://towardsdatascience.com/normalization-vs-standardization-quantitative-analysis-a91e8a79cebf
3. https://machinelearningmastery.com/avoid-overfitting-by-early-stopping-with-xgboost-in-python/
4. https://towardsdatascience.com/regularization-in-machine-learning-76441ddcf99a
5. https://en.wikipedia.org/wiki/Regularization_(mathematics)
