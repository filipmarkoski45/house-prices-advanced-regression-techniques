import numpy as np
from keras.layers import BatchNormalization
from keras.layers.core import Dense
from keras.models import Sequential, save_model, load_model
from keras.wrappers.scikit_learn import KerasRegressor

# Local Imports
from definitions import MODELS_DIR
from metrics import print_score, rmse, make_grid, timestamp

# TODO: Wrap the build_fn with KerasRegressor from keras

nb_epochs = 300

from keras import backend as K


def rmse_keras(targets, predictions):
    return K.sqrt(K.mean(K.square(predictions - targets)))


def create_relu_adam_200_100_50_25_1(optimizer='adam'):
    model = Sequential()
    model.add(Dense(200, input_dim=415,  # train.X.shape[1],
                    kernel_initializer='normal', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(100, kernel_initializer='normal', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(50, kernel_initializer='normal', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(25, kernel_initializer='normal', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(12, kernel_initializer='normal', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(1, kernel_initializer='normal'))

    # optimizer = Adadelta()
    model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=dict(rmse_keras=rmse_keras))
    return model


def relu_adam_200_100_50_25_1(train, valid=None, test=None, display_score=False, verbose=True):
    print('relu-adam-[200, 100, 50, 25, 1]')

    # Model
    model = create_relu_adam_200_100_50_25_1()

    model.fit(train.X, train.y, batch_size=64, epochs=nb_epochs, shuffle=True, validation_split=0.1, verbose=verbose)

    if display_score:
        print_score(model, train, valid, include_train=False, include_cv=False)
    return model


def relu_adam_150_1(train, valid, test, display_score=False, verbose=True):
    print('relu-adam-[150, 1]')

    # Model
    model = Sequential()
    model.add(Dense(150, input_dim=train.X.shape[1], kernel_initializer='normal', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(1, kernel_initializer='normal'))

    model.compile(loss='mean_squared_error', optimizer='adam', metrics=[rmse_keras])

    model.fit(train.X, train.y, batch_size=64, epochs=nb_epochs, shuffle=True, validation_split=0.1, verbose=verbose)

    if display_score:
        print_score(model, train, valid, include_train=False, include_cv=False)
    return model


def create_relu_adam_30_150_15_1(optimizer='adam'):
    model = Sequential()

    model.add(Dense(30, input_dim=415,  # train.X.shape[1],
                    kernel_initializer='normal', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(150, kernel_initializer='normal', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(15, kernel_initializer='normal', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(1, kernel_initializer='normal'))

    # optimizer = Adadelta()
    model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=dict(rmse_keras=rmse_keras))
    return model


def relu_adam_30_150_15_1(train, valid=None, test=None, display_score=False, verbose=True):
    print('relu-adam-[30, 150, 15, 1]')

    # Model
    model = create_relu_adam_30_150_15_1()
    model.fit(train.X, train.y, batch_size=64, epochs=nb_epochs, shuffle=True, validation_split=0.1, verbose=verbose)

    if display_score:
        print_score(model, train, valid, include_train=False, include_cv=False)

    return model


def nn_start_small_approach(train):
    print('Neural Network Approach (Keras)')

    # model = KerasRegressor(build_fn=create_relu_adam_30_150_15_1)
    # 0.2603588847038759
    # 0.16118922543360853 w/ 100 epochs (also 0.14446781396998862)
    # 0.14309145354918965 w/ 300 epochs

    keras_regressor = KerasRegressor(build_fn=create_relu_adam_200_100_50_25_1)
    # 0.2603588847038759
    # 0.1741968599567579 w/ 100 epochs
    # 0.1269605579652624 w/ 300 epochs

    # w/ added hidden layer with 12 neurons
    # 0.12671604058355013 w/ 300 epochs

    batch_size = [64]
    epochs = [300]
    optimizers = ['SGD', 'RMSprop', 'Adagrad', 'Adadelta', 'Adam', 'Adamax', 'Nadam']
    learn_rate = [0.001, 0.01, 0.1, 0.2, 0.3]
    param_grid = dict(batch_size=batch_size,
                      epochs=epochs,
                      # learn_rate=learn_rate,
                      optimizer=['Adam'],
                      validation_split=[0.1],
                      shuffle=[True],
                      verbose=[True])
    keras_regressor = make_grid(keras_regressor, train, param_grid)

    # {'batch_size': 64, 'epochs': 1000, 'optimizer': 'Adam', 'shuffle': True,
    # 'validation_split': 0.1, 'verbose': True} 0.12534680690082936
    # model = relu_adam_200_100_50_25_1(train, display_score=True)
    epochs = keras_regressor.sk_params['epochs']
    filepath = f'{MODELS_DIR}/{epochs}_epochs_relu_adam_200_100_50_25_1_{timestamp()}.h5'

    save_model(keras_regressor.model, filepath)
    return keras_regressor


def mean_score(train, valid, test):
    for u in [relu_adam_200_100_50_25_1, relu_adam_30_150_15_1]:
        lst = []
        for i in range(10):
            model = u(train, valid, test, display_score=True, verbose=False)
            score = rmse(model.predict(valid.X), valid.y)
            lst.append(score)
            if score < 0.1:
                print(f'Saving Keras model with {score}')
                model.save(f'{MODELS_DIR}/{score:.5}_{u.__name__}_i{i}.h5')
        lst = np.array(lst)

        print(f'The {u.__name__} model\'s mean values is {lst.mean()}')


def load_best_model(valid=None, display_score=False, second_best=False):
    keras_model_path = f'{MODELS_DIR}/0.093859_relu_adam_30_150_15_1_i6.h5'
    if second_best:
        keras_model_path = f'{MODELS_DIR}/0.095486_relu_adam_200_100_50_25_1_i9.h5'
    custom_objects = {'rmse_keras': rmse_keras}
    model = load_model(filepath=keras_model_path,
                       custom_objects=custom_objects)
    if display_score:
        score = rmse(model.predict(valid.X), valid.y)
        print(score)
    return model
