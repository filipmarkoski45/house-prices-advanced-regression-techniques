import pandas as pd
import pytest

# Local Imports
from definitions import serialized_train_feather
from pd_utils import downcast_dtypes


@pytest.mark.pd_utils
def test_downcast_dtypes():
    train: pd.DataFrame = pd.read_feather(serialized_train_feather)

    # Before
    print(train.info(memory_usage=True))
    before = train.memory_usage().sum()

    # After
    train = downcast_dtypes(train)
    after = train.memory_usage().sum()
    print(train.info(memory_usage='deep'))

    assert before > after

    numerical_features = train.dtypes[train.dtypes == "float32"].index
    print(len(numerical_features))
    print(numerical_features)
