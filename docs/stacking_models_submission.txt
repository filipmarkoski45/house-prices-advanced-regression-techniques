D:\code\py\house-prices-advanced-regression-techniques\venv\Scripts\python.exe D:/code/py/house-prices-advanced-regression-techniques/main.py
Execution started: 2019-07-27 09:22:45
Reading data sets from feather format...
Reading data sets from feather format...
[Parallel(n_jobs=1)]: Using backend SequentialBackend with 1 concurrent workers.
With PCA
StackingModels Approach
Fitting 5 folds for each of 1 candidates, totalling 5 fits
[Parallel(n_jobs=1)]: Done   5 out of   5 | elapsed:   26.9s finished
{} 0.10839694433312744
  params  mean_test_score  std_test_score
0     {}         0.108397        0.001378
[Parallel(n_jobs=1)]: Using backend SequentialBackend with 1 concurrent workers.
(1458, 415) (1459, 415)
StackingModels Approach
Fitting 5 folds for each of 1 candidates, totalling 5 fits
[Parallel(n_jobs=1)]: Done   5 out of   5 | elapsed:   23.7s finished
{} 0.10268602915564193
  params  mean_test_score  std_test_score
0     {}         0.102686        0.001023
Making submission.csv...
Fitting on the entire training set...
Making predictions...
Execution ended: 2019-07-27 09:24:06
Execution elapsed:  0:01:21.163045

Process finished with exit code 0
